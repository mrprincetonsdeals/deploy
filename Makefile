.PHONY: all deploy migrate new new-ssl

all: deploy

deploy: refresh-assets
	ansible-playbook --ask-vault-pass -i production --private-key ~/.ssh/mrprincetonsdeals redeploy-site.yml -e 'ansible_python_interpreter=/usr/bin/python3'

refresh-assets:
	./scripts/download-assets.sh

migrate:
	ansible-playbook --ask-vault-pass -i production --private-key ~/.ssh/mrprincetonsdeals redeploy-site.yml -e 'ansible_python_interpreter=/usr/bin/python3' --tags 'migrations'

new:
	ansible-playbook --ask-vault-pass -i production --private-key ~/.ssh/mrprincetonsdeals new-site.yml -e 'ansible_python_interpreter=/usr/bin/python3'

new-ssl:
	ansible-playbook --ask-vault-pass -i production --private-key ~/.ssh/mrprincetonsdeals ssl.yml -e 'ansible_python_interpreter=/usr/bin/python3'
