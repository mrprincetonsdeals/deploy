```
doctl compute ssh-key import new_key_name \
  --public-key ~/.ssh/id_rsa.pub
```

```
doctl compute droplet create referral-codes-web1 \
  --size s-1vcpu-2gb \
  --image ubuntu-20-04-x64 \
  --region sfo3 \
  --enable-private-networking \
  --ssh-keys "ssh:key:fingerprint:0a:0b" \
  --tag-names webserver
```

```
doctl compute droplet create referral-codes-db1 \
  --size s-1vcpu-2gb \
  --image ubuntu-20-04-x64 \
  --region sfo3 \
  --enable-private-networking \
  --ssh-keys "ssh:key:fingerprint:0a:0b" \
  --tag-names database
```

```
database-fw
webserver
5432

webserver-fw
80
443

management-fw
22

doctl compute floating-ip create --region nyc1
