#!/bin/sh

set -ex

if [ -d "roles/app/files/bin" ]
then
	rm -r roles/app/files/bin
fi

if [ -d "roles/app/files/db" ]
then
	rm -r roles/app/files/db
fi

if [ -d "roles/app/files/static" ]
then
	rm -r roles/app/files/static
fi


curl -o frontend.zip -L --header "Private-Token: $GITLAB_PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/mrprincetonsdeals%2Ffrontend/jobs/artifacts/master/download?job=build"
unzip -o frontend.zip -d roles/app/files
rm frontend.zip

curl -o backend.zip -L --header "Private-Token: $GITLAB_PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/mrprincetonsdeals%2Fbackend/jobs/artifacts/master/download?job=build"
unzip -o backend.zip -d roles/app/files
rm backend.zip
